import threading


class MultiThreads(threading.Thread):
    def __init__(self, thread_id, nums_range):
        super().__init__()
        self.thread_id = thread_id
        self.nums_range = nums_range

    def run(self):
        print(f'Thread no. {self.thread_id} starting calculating...')

    def adding(self):
        result = 0
        for x in range(self.nums_range[0], self.nums_range[1] + 1):
            result += x
        return result


class Calculate:
    def __init__(self, target_num):
        self.threads = []
        self.target_num = target_num
        self.whole_value = []

    def create_threads(self):
        parts = round(self.target_num / 100)
        size = self.target_num // parts

        for x in range(parts):
            start = x * size + 1
            stop = (x + 1) * size
            thread = MultiThreads(x + 1, (start, stop))
            self.threads.append(thread)

    def main_action(self):
        self.create_threads()
        for thread in self.threads:
            thread.start()
            action = thread.adding()
            self.whole_value.append(action)
        result = sum(self.whole_value)
        print()
        print(result)


if __name__ == '__main__':
    cal = Calculate(1000)
    cal.main_action()
